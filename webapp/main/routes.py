from flask import render_template
from flask import Blueprint

main = Blueprint('main', __name__)


@main.route("/homepage")
def homepage():
    return render_template('helloworld/homepage.html')

@main.route("/")
@main.route("/pop_up")
def pop_up():
    return render_template('helloworld/pop-up.html')


@main.route("/skim")
def skim():
    return render_template('helloworld/skim.html')


@main.route("/one")
def one():
    return render_template('helloworld/one.html')


@main.route("/twoA")
def twoA():
    return render_template('helloworld/twoA.html')


@main.route("/twoB")
def twoB():
    return render_template('helloworld/twoB.html')


@main.route("/threeA")
def threeA():
    return render_template('helloworld/threeA.html')


@main.route("/threeB")
def threeB():
    return render_template('helloworld/threeB.html')


@main.route("/fourB")
def fourB():
    return render_template('helloworld/fourB.html')


@main.route("/fiveB")
def fiveB():
    return render_template('helloworld/fiveB.html')


@main.route("/sixB")
def sixB():
    return render_template('helloworld/sixB.html')


@main.route("/seven")
def seven():
    return render_template('helloworld/seven.html')


@main.route("/eight")
def eight():
    return render_template('helloworld/eight.html')

